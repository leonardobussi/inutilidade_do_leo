matriz = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
matrizz = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
for linha in range(0, 3):
    for coluna in range(0, 3):
        matriz[linha][coluna] = int(input("digite um valor para [{0}], [{1}] ".format(linha,coluna)))
print()
#################### segunda matriz ######################
for l in range(0, 3):
    for c in range(0, 3):
        matrizz[l][c] = int(input("digite um valor para [{0}], [{1}] ".format(l,c)))
print('-=' * 30) #######################################
diagonalp = matriz[0][0] * matrizz[1][1] * matrizz[2][2] 
diagonals = matriz[0][2] * matrizz[1][1] * matrizz[2][0]
diagonalprin = matrizz[0][0] * matrizz[1][1] * matrizz[2][2] 
diagonalsegun = matrizz[0][2] * matrizz[1][1] * matrizz[2][0]
s = diagonalp +  diagonals
sk = diagonalprin + diagonalsegun
for linha in range(0, 3):
    for coluna in range(0, 3):
        print({matriz[linha][coluna]}, end='')
    print()
print("diagonal principal",diagonalp)
print("diagonal segundaria",diagonals)
print("resultado",s)
aa = matriz[0][0],matriz[0][1],matriz[0][2]
bb = matriz[0][0],matriz[1][0],matriz[2][0]
if (aa == bb):
        print("essa matriz eh simetrica")
else:
        print("essa matriz nao eh simetrica")
print("\nsegunda matriz\n")
for l in range(0, 3):
    for c in range(0, 3):
        print({matrizz[l][c]}, end='')
    print()  
print("diagonal principal",diagonalprin)
print("diagonal segundaria",diagonalsegun)
print("resultado",sk)
##################################
aaa = matrizz[0][0],matrizz[0][1],matrizz[0][2]
bbb = matrizz[0][0],matrizz[1][0],matrizz[2][0]
if (aaa == bbb):
        print("essa matriz eh simetrica")
else:
        print("essa matriz nao eh simetrica")
#################################################
soma1 = matriz[0][0] + matrizz[0][0]
soma2 = matriz[0][1] + matrizz[0][1]
soma3 = matriz[0][2] + matrizz[0][2]
soma4 = matriz[1][0] + matrizz[1][0]
soma5 = matriz[1][1] + matrizz[1][1]
soma6 = matriz[1][2] + matrizz[1][2]
soma7 = matriz[2][0] + matrizz[2][0]
soma8 = matriz[2][1] + matrizz[2][1]
soma9 = matriz[2][2] + matrizz[2][2]
print("\na soma entre as matrizes 3x3 deu:")
print("[{0},  {1},  {2}]\n[{3},  {4},  {5}]\n[{6},  {7},  {8}]".format(soma1,soma2,soma3,soma4,soma5,soma6,soma7,soma8,soma9))
a = soma1, soma2, soma3
b = soma1, soma4, soma7
if (a == b):
        print("essa matriz eh simetrica")
else:
        print("nao eh simetrica")
################################################
print("=======================================")
soma10 = matriz[0][0] - matrizz[0][0]
soma20 = matriz[0][1] - matrizz[0][1]
soma30 = matriz[0][2] - matrizz[0][2]
soma40 = matriz[1][0] - matrizz[1][0]
soma50 = matriz[1][1] - matrizz[1][1]
soma60 = matriz[1][2] - matrizz[1][2]
soma70 = matriz[2][0] - matrizz[2][0]
soma80 = matriz[2][1] - matrizz[2][1]
soma90 = matriz[2][2] - matrizz[2][2]
print("\na subtracao entre as matrizes 3x3 deu:")
print("[{0},  {1},  {2}]\n[{3},  {4},  {5}]\n[{6},  {7},  {8}]".format(soma10,soma20,soma30,soma40,soma50,soma60,soma70,soma80,soma90))
a1 = soma10, soma20, soma30
b1 = soma10, soma40, soma70
if (a1 == b1):
        print("essa matriz eh simetrica")
else:
        print("nao eh simetrica")
soma100 = matriz[0][0] * matrizz[0][0]
soma200 = matriz[0][1] * matrizz[0][1]
soma300 = matriz[0][2] * matrizz[0][2]
soma400 = matriz[1][0] * matrizz[1][0]
soma500 = matriz[1][1] * matrizz[1][1]
soma600 = matriz[1][2] * matrizz[1][2]
soma700 = matriz[2][0] * matrizz[2][0]
soma800 = matriz[2][1] * matrizz[2][1]
soma900 = matriz[2][2] * matrizz[2][2]
print("\na multiplicacao entre as matrizes 3x3 deu:")
print("[{0},  {1},  {2}]\n[{3},  {4},  {5}]\n[{6},  {7},  {8}]".format(soma100,soma200,soma300,soma400,soma500,soma600,soma700,soma800,soma900))
a2 = soma100, soma200, soma300
b2 = soma100, soma400, soma700
if (a2 == b2):
        print("essa matriz eh simetrica")
else:
        print("nao eh simetrica")
soma11 = matriz[0][0] / matrizz[0][0]
soma22 = matriz[0][1] / matrizz[0][1]
soma33 = matriz[0][2] / matrizz[0][2]
soma44 = matriz[1][0] / matrizz[1][0]
soma55 = matriz[1][1] / matrizz[1][1]
soma66 = matriz[1][2] / matrizz[1][2]
soma77 = matriz[2][0] / matrizz[2][0]
soma88 = matriz[2][1] / matrizz[2][1]
soma99 = matriz[2][2] / matrizz[2][2]
print("\na divisao entre as matrizes 3x3 deu:")
print("[{0},  {1},  {2}]\n[{3},  {4},  {5}]\n[{6},  {7},  {8}]".format(soma11,soma22,soma33,soma44,soma55,soma66,soma77,soma88,soma99))
a3 = soma11, soma22, soma33
b3 = soma11, soma44, soma77
if (a3 == b3):
        print("essa matriz eh simetrica")
else:
        print("nao eh simetrica")