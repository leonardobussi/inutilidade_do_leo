def fatorial (numero):
    if numero == 0 or numero == 1:
        return 1
    else:
        return numero * fatorial(numero -1)
a = int(input("digite um numero para calcular seu fatorial\n"))
res = fatorial(a)
print("o fatorial de %d e %d" % (a,res))
