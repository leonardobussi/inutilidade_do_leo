#!/usr/bin/python
# -*- encoding: utf-8 -*-


def imprime_lista(lista):
    for v in lista:
        print v


def criar_lista():
    lista = []
    n = int(raw_input("Informe a quantidade de vertices:"))
    for i in range(n):
        lista.append([i])
    return lista


def insere_aresta(lista):
    v1, v2 = raw_input("Informe a aresta: ").split()
    v1 = int(v1)
    v2 = int(v2)
    lista[v1].append(v2)
    lista[v2].append(v1)
    imprime_lista(lista)


def adjacentes_vertice(v, lista):
    adj = []
    for i in range(len(lista)):
        if (lista[i][0] == v):
            adj = lista[i][1:]
    return adj


def existe_aresta(lista):
    v1, v2 = raw_input("Informe a aresta: ").split()
    v1 = int(v1)
    v2 = int(v2)
    if ((v1 in adjacentes_vertice(v2, lista)) or (v2 in adjacentes_vertice(v1, lista))):
        return True
    else:
        return False


def grau_vertice(v, lista):
    grau = len(adjacentes_vertice(v, lista))
    for vizinho in adjacentes_vertice(v, lista):
        if (vizinho == v):
            grau += 1
    return grau


menu = {}
menu['1'] = "Criar Lista"
menu['2'] = "Insere Aresta"
menu['3'] = "Visualiza Lista"
menu['4'] = "Vertices Adjacentes"
menu['5'] = "Existe Aresta Entre"
menu['6'] = "Grau do Vertice"
menu['7'] = "Sair"
grafo = []
while True:
    options = menu.keys()
    options.sort()
    print '\n'
    for entry in options:
        print entry, menu[entry]
    selection = raw_input("Selecione a opção:")
    if selection == '1':
        print 'cria lista'
        grafo = criar_lista()
        imprime_lista(grafo)
    elif selection == '2':
        print 'insere aresta'
        insere_aresta(grafo)
    elif selection == '3':
        print'visualiza lista'
    elif selection == '4':
        print 'vertices adjacentes'
        v = int(raw_input("Informe o vertice: "))
        adj = adjacentes_vertice(v, grafo)
        print 'vertices adjacentes de ', v, ' sao => ', adj
    elif selection == '5':
        print 'existe aresta? ', existe_aresta(grafo)
    elif selection == '6':
        print 'grau do vertice'
        v = int(raw_input("Informe o vertice: "))
        grau = grau_vertice(v, grafo)
        print 'grau de ', v, ' é => ', grau
    elif selection == '7':
        break
    else:
        print "Opção selecionada INVÁLIDA!"