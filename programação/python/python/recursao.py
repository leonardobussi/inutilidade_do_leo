n = int(input("digite o valor: "))



def soma(n):
    if n == 0:
        return 0
    else:
        return soma(n-1) + n
print(" ")
print(soma(n))
print(" ")
print("-"*50)



print(" ")
def fatorial(n):
    if n in [0, 1]:
        return 1
    else:
        return fatorial(n - 1) * n
print(f"fatorial {n} é {fatorial(n)}")
print(" ")
print("-"*50)




print(" ")
def conta_pares(n):
    if n == 0: return 1 # 0 é par
    elif n%2 == 0: return 1 + conta_pares(n-1)
    else: return conta_pares(n-1)
print(conta_pares(n))
print(" ")
print("-"*50)




print(" ")
def conta_pares_it(n):
    p = 0 
    for num in range(n+1):
        if num%2 == 0:
            p += 1
    return p
print(conta_pares_it(n))
print(" ")
print("-"*50)



print(" ")
def conta_pares_py(n):
   return len([p for p in range(n+1) if p%2 == 0])
print(conta_pares_py(n))
print(" ")
print("-"*50)


print(" ")
def fib(n):
    if n in [0, 1]: return n
    else: return fib(n-1)+fib(n-2)
print(f"fibonacci de {n} é {fib(n)}")
print(" ")
print("-"*50)


print(" ")
def fib_it(n):
    i, j = 1, 0
    for k in range(1, n + 1):
        i, j = j, i + j
    return j
print(f"fibonacci de {n} é {fib_it(n)}")
print(" ")
print("-"*50)


print(" ")
def fib_cache(n):
    if n in [0, 1]: return n
    else: return fib(n-1) + fib(n-2)
print(f"fibonacci de {n} é {fib_cache(n)}")
print(" ")
print("-"*50)