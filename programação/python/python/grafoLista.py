def imprime_lista(lista):
    for v in lista:
        print(v)


def criar_lista():
    lista = []
    n = int(input("informe a quantidade de vertices: "))
    for i in range(n):
        lista.append([i])
    return lista

def insere_aresta(lista):
    v1, v2 = input("informe as aresta: ").split()
    v1 = int(v1)
    v2 = int(v2)
    lista[v1].append(v2)
    lista[v2].append(v1)
    imprime_lista(lista)

def adjacentes_vertices(v, lista):
    adj = []
    for i in range(len(lista)):
        if lista[i][0] == v:
            adj = lista[i][1:]
    return adj

def existe_aresta(lista):
    v1, v2 = input("informe a aresta: ").split()
    v1 = int(v1)
    v2 = int(v2)
    if v1 in adjacentes_vertices(v2, lista) or v2 in adjacentes_vertices(v1, lista):
        return True
    else:
        return False

def grau_vertice(v, lista):
    grau = len(adjacentes_vertice(v, lista))
    for vizinho in adjacentes_vertice(v, lista):
        if vizinho == v:
            grau += 1
    return grau

grafo = criar_lista()
imprime_lista(grafo)
insere_aresta(grafo)
v = int(input("Informe o vertice: "))
adj = adjacentes_vertice(v, grafo)
print (f"vertices adjacentes de {v}  sao   {adj}")
print (f"existe aresta?  {existe_aresta(grafo)}")
print ('grau do vertice')
v = int(input("Informe o vertice: "))
grau = grau_vertice(v, grafo)
print (f"grau de {v} é  {grau}")