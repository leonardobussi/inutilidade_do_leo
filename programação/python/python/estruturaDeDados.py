entidades = dict()

entidades = {
    'Instituicao': [
        ('IdInstituicao', 'bigint',
        'Identificador da instituição-PK'),
        ('IdTipoInstituicao', 'bigint',
        'Id do tipo de instituição'),
        ('NomInstituicao', 'varchar', 'Nome da instituição'),
        ('NumCnpj', 'varchar', 'Número do CNPJ')
    ]
}
print(entidades)

