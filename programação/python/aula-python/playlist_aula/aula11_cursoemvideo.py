print("\n\n\033[37mola, mundo!\n\n\n\033[m")
a = 3
b = 5
print("\033[7mos valores sao:\033[m \033[33m{0}\033[m e \033[31m{1}\033[m!!! \n\n\n".format(a,b))

# criando lista de cores
nome = "Leonardo"
snome = "Bezerra"
sobnome = "Bussi"
cores = {"limpa": "\033[m", "azul": "\033[34m", "amarelo": "\033[33m", "pretoebranco": "\033[7;30m"}
print("\033[7molá! muito prazer em te conhecer\033[m, {0}{1}{2} {6} {3}{4}{5} !!\n\n".format(cores["azul"], nome, cores["limpa"], cores["amarelo"], sobnome, cores["limpa"], snome))