#funçoes matematicas
import math
#pi
print (math.pi)
#raiz quadrada
print("\n")
print (math.sqrt(81))
# ou
a = 81
print (math.sqrt(a))
# valor absoluto
print("\n")
c = -5
print (math.fabs(c))
# fatorial
print("\n")
print (math.factorial(6))
# ou
d = 6
print (math.factorial(d))
# logaritmo na base 10
print("\n")
print (math.log10(8))
e = 8
print (math.log10(e))
#exponencial
print("\n")
print (math.pow(3,2))
# ou
f = 3
g = 2
print (math.pow(f,g))
# ou
h = math.pow(f,g) #ou h = math.pow(3,2)
print(h)
# arredondamento para cima
print("\n")
i = 8/5
print (i)
print (math.ceil(i))
# arrendonda para baixo
print (math.floor(i))
# teste log10
print("\n")
j =math.log10(math.sqrt(math.pow(2,7) - 2 ** 2 * 7))
print(j)