#aula 16
print("----------aula 16----------")
#sequencia 
#criaçao de lista
a = []# lista vazia
b = [0,2,5,3,4,1]
print(list(b))#printando conteudo da lista da variavel b
c = [5,6,7,8]
print(list(c))#printando conteudo da lista da variavel b
print(list(b *5))#fazendo a multiplicaçao do conteudo da lista (repetiçao)
b.append(5)#adiciona mais um certo conteudo para lista da variavel (aqui no caso é a variavel 'b')
print(list(b))
b.insert(3, 12)#adiciona o conteudo na posiçao indicada, mas essa funçao nao substitiu, mas sim adiciona 
print(list(b))
d = c.index(6)#mostra em qual posicao esta o conteudo em que procuro
print(d)
e = b.count(3)#mostra quantas vezes ocorre esse conteudo
print(e)
##################################
#aula17
print("--------aula 17---------")
b.sort()#coloca em ordem do menor para o maior
print(b)
b.reverse()#coloca em ordem do maior para o menor
print(b)
b.remove(2)#remove o objeto que eu indica no parentes
print(b)
b.pop(2)#remove o item na posiçao de indice especificada
print(b)
del b [1]#remove o item na posiçao de indice especificada
print(b)
del b[1:4]#remove os itens da posicao i ate j-1 (ex: 1:4 (1,2,3,4,5)sera removido o 2,3,4 o cinco nao pois é j-1)
print(b)
b[1] = 22#substitui o valor do indice indicado
print(b)
b[0:1] = [33,44]#substitui os item de i a j-1 por outro valor determinado
print(b)
bb = [x + 1 for x in b]##incrementa +1 a cada item na lista
print(bb)
bbb = [x ** 10 for x in b]#incrementa esponencial elevado a 10, para cada item da lista
print(bbb)
print(len (b))
