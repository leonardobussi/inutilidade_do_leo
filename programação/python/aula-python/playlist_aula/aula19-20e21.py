#dicionários
#{} para codificar com chaves
#[] para retorna o valor associado à chave produto
print("----------aula 19----------")
a = {"produto": "tigela", "cor": "azul", "preço": 14}
print(a["cor"])
b = {}
b ["nome"] = "leonardo"
b ["sobrenome"] = "bussi"
b ["esporte"] = "skate"
print(b["nome"])
print(b["sobrenome"])
print(b["esporte"])
###################################
#aula 20
print("--------aula 20---------\n")
#aninhamento em dicionários
c = {"nome": {"primeiro": "leonardo", "ultimo": "bezerra bussi"}, "conhecimento":{"python", "c++"}, "idade": 20}
print(c["nome"])
print(c["conhecimento"])
print(c["nome"]["primeiro"])
###################################
#aula21
#ordenaçao de dicionarios
print("---------aula 21 ---------\n")
D = {"b": 2, "a": 1, "d": 4, "c": 3}
ordenada = list(D.keys())
ordenada.sort()
for key in ordenada:
   print(key, "=", D[key])
print("\n")
for key in sorted(D):
    print(key, "=", D[key])