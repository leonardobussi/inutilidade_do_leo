#loop for
print("---------------")
var = "python"
for x in var:
    print(x)
frutas =["laranja","morango","quanrana","açai"]
for y in frutas:
    print("fruta: %s" % y)
#loop for com contador
print("---------------")
#range(inicio, fim, salto)
#range(11) - valores de 0 a 10
#range(5,11) - valores de 5 a 10
#range(2,50,2) - valores de 2 a 50 saltando de 2 em 2
for f in range(11):
    print(f)
print("--------")
for g in range(0,51,5):
    print(g)