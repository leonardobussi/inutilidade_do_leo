#gerando numeros aleatorios
import random
l = [2,4,6,8,10,12,14,16,18,20]
print("gerando numeros aleatorios de 1 a 50\n")
for a in range(1,6):
    b = random.randint(1,50)
    print("o numero gerado e: ", b)
print("------------------------")
print("gerando numeros aleatorios de 1 a 50 em ponto flutuante\n")
for c in range(1,6):
    d = random.uniform(1,100)
    print("o numero gerado e: ", d)
print("-----------------------")
print("extraindo numero aleatorio da lista:\n")
e = random.choice(l)
print("o numero gerado e: ", e)
print("------------------------")
print("extraindo numeros aleatorios da lista\n")
f = random.sample(l,3)
print("os numeros gerados e: ", f)