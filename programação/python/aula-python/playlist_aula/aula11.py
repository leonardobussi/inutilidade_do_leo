#metedos em Strings
s = "   pizza   "
ss = "a,b,c,d,e,f,g,h"
#metedo find
print(s.find("zz"))#indica a posiçao da palavra ou letra descrita
#metedo replace
print(s.replace("zza", "nhao")) #substitui 
#metedo upper
print(s.upper()) #deixa tudo maiusculo
#metedo lower
print(s.lower()) #deixa tudo minusculo
#metedo isalpha
print(s.isalpha())#teste de conteudo (é alfabetica)
#metedo isalnum
print(s.isalnum())#teste de conteudo (é alfanumerico)
#metedo lstrip, rstrip e stip
print(s.lstrip())#limpa todo espaço em branco a esquerda
print(s.rstrip())#limpa todo espaço em branco a direita
print(s.strip())#limpa todo espaço em branco quanto da esquerda e da direita
print(s.lstrip("p"))# limpa e remove o espaço em branco junto do caractere especifico esquerdo
print(s.rstrip("a"))#limpa e remove o espaço em branco junto do caractere especifico a direita
#metedo capitalize
print(s.capitalize())#coloca a primeira letra em maiuscula uso para formataçao de texto
#metedo split
print(ss.split(","))