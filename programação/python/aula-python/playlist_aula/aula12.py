#funçao print
a = "pizza"
b = 30
print(a)
#metedo %s(string) e %d (numero inteiro)
print("gosto de %s" % a)
print("custa 30 reais")
print("custa %d reais" % b)
print("a %s custa %d reais" % (a,b))
#metedo str.format
print("gosto de {0}, mas {1} reais esta muito caro para uma {0}".format(a, b))
