def somar(a=0, b=0, c=0):
    d = a + b + c
    print("a soma deu: {0}".format(d))

def somar2(a=0, b=0, c=0):
    d = a + b + c
    return d

somar(3,4,5)
print()
somar(3,5)
print()
somar(5)

r1 = somar2(2, 4, 3)
r2 = somar2(2, 4)
r3 = somar2(2)
print("\n\n")
print("a soma de r1 deu: {0}, r2 deu {1} e r3 deu: {2} ".format(r1,r2,r3))