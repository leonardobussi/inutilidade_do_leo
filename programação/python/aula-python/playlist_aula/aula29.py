#recursividade
def fatorial (numero):
    if numero == 0 or numero == 1:
        return 1
    else:
        return numero * fatorial(numero -1)
x = int(input("digite um numero para calcular seu fatorial\n"))
res = fatorial(x)
print("o fatorial de %d e %d" % (x,res))
#fibonacci recursivo
def fibonacci(num):
    if num <= 1:
        return num
    else:
        return fibonacci(num -1) + fibonacci(num - 2)
y = int(input("digite um numero\n"))
rest = fibonacci(y-1)
print("o fibonacci de %d e %d" % (y,rest))