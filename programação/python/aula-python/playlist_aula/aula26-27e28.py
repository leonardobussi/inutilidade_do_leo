#funçoes escopo das variaveis local e global
var_global = "leo e da hora"
def escreve_texto():
    global var_global
    var_global = "leo nao e da hora"
    var_local = "palmeiras"
    print("variavel global:b ", var_global)
    print("variavel local: ", var_local)
print("execultando ------")
escreve_texto()
print("tentando acessar-------")
print("variavel global: ", var_global)
################
print("--------aula 27-----")
#aula 27
def contar (valor=11, caractere="+"):
    for i in range(1,valor):
        print(caractere)
contar()
print("-----difrente------")
contar (caractere="&")
print("--------outro-------")
contar(valor=5)
##################
print("--------------------------------")
def co (va=11, cara="+"):
    for v in range(1,va):
        print(cara)
co()
print("passando:")
co(6,"$")
print("---------------------------------")
print("-------------outro parametro---------------")
co(cara="p",va= 7)
##############################################
#aula 28
#empacotamento e desempacotamento
print("-------------aula 28---------------")
def listar_itens(w,x,y,z):
        print(w,x,y,z)
lista = [21,22,67,69]
listar_itens(*lista)
print("------empacota-----")
def somar(*a):
        soma=0
        for b in range(0, len(a)):
                soma += a [b]
        return soma
print(somar(1,2,3,4,5,6,7,8,910))
print(somar(77,23))