def titulo (txt):
    print()
    print("-"*30)
    print(txt)
    print("-"*30)

def soma (a, b):
    print()
    print("-"*30)
    s = a + b
    print(s)
    print("-"*30)

def dobra(lst):
    pos = 0
    while pos < len(lst):
        lst[pos] *= 2
        pos += 1

def soma2(* valor):
    s = 0
    for num in valor:
        s += num
    print("a soma dos valores {0} temos {1}".format(valor, s))       


titulo('ola mundo')
soma(1,3)
soma(33,193)
valor = [6, 3, 9, 1, 0, 2]
dobra(valor)
print(valor)
print("\n\n")
soma2(5, 2)
soma2(2, 9, 4)