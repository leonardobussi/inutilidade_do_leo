# dados Strings
a = "pizza"
print(a)
#funçao para identificar quantos caracter tem no testo
s = "pizza"
x = len(s)
print(x)
# alem de identifica podemos imprimir o caracter individual
print(s[0])
print(s[1])
print(s[2])
print(s[3])
print(s[4])
# Slicing ("fatiamento")
print (s[:])#imprime tudo
print (s[1:3])#imprime somente o caracter 1 e 3 lembrando que começa do zero
print (s[:4])#nao imprime o ultimo caractere
print (s[2:])#corta as duas primeiras letras, so imprimi as 3 ultimas
print (s[-1])# de traz pra frente. ex: pizz'a' pra entender melhor 'a' 'z' 'z' 'i' 'p'
# concatenaçao
print (s + " portuguesa")
#repeticao
print ((s + " ") * 6)
#teste
b = "linux"
print (b[:3])
