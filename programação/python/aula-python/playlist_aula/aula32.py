#abrindo arquivo de texto
#metedo read() - retorna o conteudo do arquivo como uma única string
#metedo readline() - retorna uma linha do texto a cada chamada, na ordem em que aparecem no arquivo (um ponteiro de linha é incrementado a cada nova chamada ao métedo).
#métedo readlines() - retorna uma lista de valores de string do arquivo, sendo que cada string corresponde        texto
#é mais facil trabalhar o métedo readlines() do que com o métedo read().
#exemplo:
a = open("textobom.txt", "r")
print("\n metedo read():\n")
print(a.read())
a.seek(0)#volta para o inicio do arquivo
print("----------")
print("metedo readline():\n")
print(a.readline())
print(a.readline())
a.seek(0)
print("----------")
print("\nmetedo readlines():\n")
print(a.readlines())
a.close()