#include <stdio.h>
int main ()
{
    double firstNumber, secondNumber, a;
    printf("enter first number: ");
    scanf("%lf", & firstNumber);

    printf("enter second number: ");
    scanf("%lf", &secondNumber);

    a = firstNumber;
    firstNumber = secondNumber;
    secondNumber = a;

    printf("\nafter swapping, firsNumber = %.2lf\n",firstNumber);
    printf("after swapping, secondNumber = %.2lf\n", secondNumber);

    return 0;
}