#include <stdio.h>
#include <math.h>
#include <stdlib.h>
int main ()
{
    double a,b,c, determinante, root1,root2, realpart,imaginarypart, raizDeter;
    printf("entre com o coeficiente a, b, c: ");
    scanf("%lf %lf %lf", &a,&b,&c);

    determinante = b*b - 4 * a * c;
     raizDeter = sqrt(determinante);

    if (determinante > 0)
    {
  
        root1 = (-b + raizDeter) / (2*a);
        root2 = (-b - raizDeter)  / (2*a);
        
        printf("root1 = %.2lf and root2 = %.2lf", root1, root2);
    }
    else if (determinante == 0)
    {
        root1 = root2 = -b/(2*a);

        printf("root1 = root2 = %.2lf", root1);
    }
    else
     {
        realpart = -b/(2*a);
        imaginarypart  =  sqrt(-determinante)/(2*a);
        printf("root1 = %.2lf+%.2lfi and root2 = %.2f-%.2fi", realpart, imaginarypart, realpart, imaginarypart);
    }
    return 0;
}
