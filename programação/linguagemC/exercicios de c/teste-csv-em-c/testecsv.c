#include <stdio.h>
#include <stdlib.h>
#define ZEILEN  3 
#define SPALTEN 5 
#define SEP ","
void uebergabe(int array[][SPALTEN],int zeile, int spalte, const char *spaltestr)
{
array [zeile][spalte] = atoi(spaltestr);
printf("%s%3d%s",spalte?"":"\n",array[zeile][spalte],SEP);
}
int main ()
{
    FILE *f = fopen("teste.csv","r");
    if(f)
    {
        int array [ZEILEN][SPALTEN] = {0};
        int  n, zeile,spalte;
        char zeilestr [100],spaltestr[10],*p;
        for (zeile=0; zeile <ZEILEN && fgets(zeilestr,100,f);++zeile)
        for (spalte = 0,n=*spaltestr=0,p=zeilestr;
        spalte <SPALTEN && sscanf(p, "%9[^"SEP"]%n",spaltestr,&n)!=EOF;
        ++spalte, p+=n, *p?++p:0)
        uebergabe(array,zeile,spalte,spaltestr);
        fclose(f);  ("\n"); //put
    }
   else
        return 1;
    return 0;
}